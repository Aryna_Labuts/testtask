package com.labuts.catalog.dao;

import com.labuts.catalog.configuration.SpringConfig;
import com.labuts.catalog.entity.Rate;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertEquals;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {SpringConfig.class})
public class ExchangeRateDaoImpl {

    @Autowired
    private ExchangeRateDao exchangeRateDao;

    @Test
    public void findLatestRateCoefficientTest(){
        double expectedCoefficient = 2069.829767;

        double actualCoefficient = exchangeRateDao.findLatestRateCoefficient(Rate.BIF);
        assertEquals(expectedCoefficient, actualCoefficient, 0.000001);
    }
}
