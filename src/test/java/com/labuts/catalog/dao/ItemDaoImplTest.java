package com.labuts.catalog.dao;

import com.labuts.catalog.configuration.SpringConfig;

import com.labuts.catalog.entity.Item;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertEquals;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {SpringConfig.class})
public class ItemDaoImplTest {

    @Autowired
    private ItemDao itemDao;


    @Test
    public void findAllTest(){
        int expectedListSize = 3;
        int actualListSize = itemDao.findAll().size();
        assertEquals(expectedListSize, actualListSize);
    }

    @Test
    public void findByIdTest(){
        double expectedPrice = 35.50;
        String expectedName = "Second item";

        int id = 2;
        Item foundItem = itemDao.findById(id);
        if(foundItem == null){
            foundItem = new Item();
            foundItem.setPrice(0);
            foundItem.setName("");
        }
        assertEquals(expectedPrice, foundItem.getPrice(), 0.001);
        assertEquals(expectedName, foundItem.getName());
    }

}
