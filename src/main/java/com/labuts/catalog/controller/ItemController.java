package com.labuts.catalog.controller;

import com.labuts.catalog.configuration.SpringConfig;
import com.labuts.catalog.entity.dto.ItemDto;
import com.labuts.catalog.service.ItemService;
import com.labuts.catalog.service.exception.IncorrectDataException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;

@RestController
@Import(SpringConfig.class)
public class ItemController {

    private final ItemService itemService;

    @Autowired
    public ItemController(ItemService itemService) {
        this.itemService = itemService;
    }


    @GetMapping(value = "/items", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<ItemDto>> allItemsList(@RequestParam(name = "rate", required = false) String rate){
        List<ItemDto> items;
        if(rate == null){
            items = itemService.findAll();
        }else {
            items = itemService.findAllInChosenRate(rate);
        }
        if(items.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(items, HttpStatus.OK);
    }

    @GetMapping(value = "/items/{id:\\d{1,10}}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ItemDto> getItem(@PathVariable("id") long id){
        ItemDto item = itemService.findById(id);
        if(item == null){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(item, HttpStatus.OK);
    }

    @PostMapping(value = "/items", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> createItem(@RequestBody ItemDto itemDto) throws IncorrectDataException {
        if(itemDto == null){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } else {
            itemService.insert(itemDto);
            return new ResponseEntity<>(HttpStatus.CREATED);
        }
    }

    @PutMapping(value = "items/{id:\\d{1,10}}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ItemDto> updateItem(@PathVariable("id") long id,
                                              @RequestBody ItemDto itemDto) throws IncorrectDataException {
        if(itemDto == null){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }else {
            ItemDto result = itemService.update(id, itemDto);
            return new ResponseEntity<>(result, HttpStatus.OK);
        }
    }

    @DeleteMapping(value = "items/{id:\\d{1,10}}")
    public ResponseEntity<Void> deleteItem(@PathVariable("id") long id){
        itemService.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @ExceptionHandler(IncorrectDataException.class)
    public HashMap<String, String> incorrectValue(Exception e)
    {
        HashMap<String, String> map = new HashMap<>();

        map.put("success", "false");
        map.put("message", e.getMessage());

        return map;
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public HashMap<String, String> incorrectRate(Exception e)
    {
        HashMap<String, String> map = new HashMap<>();

        map.put("success", "false");
        map.put("code", "422");
        map.put("message", e.getMessage());

        return map;
    }
}
