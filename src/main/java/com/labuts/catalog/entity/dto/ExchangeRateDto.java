package com.labuts.catalog.entity.dto;

import com.labuts.catalog.entity.Rate;
import lombok.*;

import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class ExchangeRateDto {
    private Rate rate;
    private double coefficient;
    private Date date;
}
