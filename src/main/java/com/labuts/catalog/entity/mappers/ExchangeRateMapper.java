package com.labuts.catalog.entity.mappers;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.Map;

@Setter
@Getter
public class ExchangeRateMapper {
    private Date date;
    private Map<String, Double> rates;
}
