package com.labuts.catalog.dao;

import com.labuts.catalog.entity.ExchangeRate;
import com.labuts.catalog.entity.Rate;

public interface ExchangeRateDao extends AbstractDao<ExchangeRate>{
    double findLatestRateCoefficient(Rate rate);
}
