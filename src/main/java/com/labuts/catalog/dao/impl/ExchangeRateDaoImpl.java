package com.labuts.catalog.dao.impl;

import com.labuts.catalog.entity.ExchangeRate;
import com.labuts.catalog.dao.ExchangeRateDao;
import com.labuts.catalog.entity.Rate;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public class ExchangeRateDaoImpl implements ExchangeRateDao {

    private static final String ALL_EXCHANGE_RATES_QUERY = "SELECT exr FROM ExchangeRate exr";
    private static final String LATEST_EXCHANGE_RATE_COEFFICIENT_QUERY = "select distinct exr.coefficient from ExchangeRate exr where exr.rate=:rate and exr.date= (select max(exr1.date) from ExchangeRate exr1)";

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public ExchangeRate update(ExchangeRate entity) {
        return entityManager.merge(entity);
    }

    @Override
    public List<ExchangeRate> findAll() {
        TypedQuery<ExchangeRate> query = entityManager.createQuery(ALL_EXCHANGE_RATES_QUERY, ExchangeRate.class);
        return query.getResultList();
    }

    @Override
    public ExchangeRate findById(long id) {
        return entityManager.find(ExchangeRate.class, id);
    }

    @Override
    public void insert(ExchangeRate entity) {
        entityManager.persist(entity);
    }

    @Override
    public void delete(ExchangeRate entity) {
        entityManager.remove(entityManager.contains(entity) ? entity : entityManager.merge(entity));
    }

    @Override
    public double findLatestRateCoefficient(Rate rate) {
        TypedQuery<Double> query = entityManager.createQuery(LATEST_EXCHANGE_RATE_COEFFICIENT_QUERY, Double.class);
        query.setParameter("rate", rate);
        return query.getSingleResult();
    }
}
