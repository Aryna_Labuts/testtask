package com.labuts.catalog.dao.impl;

import com.labuts.catalog.entity.Item;
import com.labuts.catalog.dao.ItemDao;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public class ItemDaoImpl implements ItemDao {

    private static final String ALL_ITEMS_QUERY = "SELECT item FROM Item item";

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Item update(Item entity) {
        return entityManager.merge(entity);
    }

    @Override
    public List<Item> findAll() {
        TypedQuery<Item> query = entityManager.createQuery(ALL_ITEMS_QUERY, Item.class);
        return query.getResultList();
    }

    @Override
    public Item findById(long id) {
        return entityManager.find(Item.class, id);
    }

    @Override
    public void insert(Item entity) {
        entityManager.persist(entity);
    }

    @Override
    public void delete(Item entity) {
        entityManager.remove(entityManager.contains(entity) ? entity : entityManager.merge(entity));
    }
}
