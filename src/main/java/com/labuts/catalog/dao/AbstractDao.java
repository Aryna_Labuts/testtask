package com.labuts.catalog.dao;

import java.util.List;

public interface AbstractDao<T> {
    T update(T entity);
    List<T> findAll();
    T findById(long id);
    void insert(T entity);
    void delete(T entity);
}
