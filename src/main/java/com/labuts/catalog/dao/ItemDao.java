package com.labuts.catalog.dao;

import com.labuts.catalog.entity.Item;

public interface ItemDao extends AbstractDao<Item> {
}
