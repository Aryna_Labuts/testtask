package com.labuts.catalog.configuration.datasource;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.jpa.vendor.Database;

import javax.sql.DataSource;

@Configuration
@ComponentScan({"com.labuts.catalog.dao", "com.labuts.catalog.service"})
public class HikariDatasourceConfig {

    @Bean(name = "dataSource")
    public static DataSource getDataSource() {
        HikariConfig config = new HikariConfig();
        config.setJdbcUrl( "jdbc:mysql://localhost:3306/item_catalog?serverTimezone=UTC" );
        config.setUsername( "root" );
        config.setPassword( "0506" );
        config.setDriverClassName("com.mysql.jdbc.Driver");
        config.addDataSourceProperty( "cachePrepStmts" , "true" );
        config.addDataSourceProperty( "prepStmtCacheSize" , "250" );
        config.addDataSourceProperty( "prepStmtCacheSqlLimit" , "2048" );
        return new HikariDataSource(config);
    }

    @Bean
    public Database getDatabase(){
        return Database.MYSQL;
    }
}
