package com.labuts.catalog.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.labuts.catalog.configuration.persistence.PersistenceConfig;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.scheduling.annotation.EnableScheduling;

@Configuration
@EnableScheduling
@Import({PersistenceConfig.class})
public class SpringConfig{
    @Bean
    public ModelMapper modelMapper(){
        return new ModelMapper();
    }

    @Bean
    public ObjectMapper objectMapper() { return new ObjectMapper();}

}
