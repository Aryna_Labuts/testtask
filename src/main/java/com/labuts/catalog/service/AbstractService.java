package com.labuts.catalog.service;

import com.labuts.catalog.service.exception.IncorrectDataException;

import java.util.List;

public interface AbstractService<T> {
    List<T> findAll();
    T findById(long id);
    void delete(long id);
    T update(long id, T entity) throws IncorrectDataException;
    void insert(T entity) throws IncorrectDataException;
}
