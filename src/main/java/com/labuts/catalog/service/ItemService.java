package com.labuts.catalog.service;

import com.labuts.catalog.entity.dto.ItemDto;

import java.util.List;

public interface ItemService extends AbstractService<ItemDto> {
    List<ItemDto> findAllInChosenRate(String rate);
}
