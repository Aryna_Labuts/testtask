package com.labuts.catalog.service.impl;

import com.labuts.catalog.entity.ExchangeRate;
import com.labuts.catalog.entity.Rate;
import com.labuts.catalog.entity.dto.ExchangeRateDto;
import com.labuts.catalog.dao.ExchangeRateDao;
import com.labuts.catalog.service.ExchangeRateService;
import com.labuts.catalog.service.exception.IncorrectDataException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ExchangeRateServiceImpl implements ExchangeRateService {

    private final ExchangeRateDao exchangeRateDao;
    private final ModelMapper modelMapper;

    @Autowired
    public ExchangeRateServiceImpl(ExchangeRateDao exchangeRateDao, ModelMapper modelMapper) {
        this.exchangeRateDao = exchangeRateDao;
        this.modelMapper = modelMapper;
    }

    @Override
    public List<ExchangeRateDto> findAll() {
        List<ExchangeRate> exchangeRates = exchangeRateDao.findAll();
        return exchangeRates.stream().map(this::convertToExchangeRateDto).collect(Collectors.toList());
    }

    @Override
    public ExchangeRateDto findById(long id) {
        ExchangeRate exchangeRate = exchangeRateDao.findById(id);
        return exchangeRate == null ? null : convertToExchangeRateDto(exchangeRate);
    }

    @Override
    public void delete(long id) {
        ExchangeRate exchangeRateToDelete = new ExchangeRate();
        exchangeRateToDelete.setId(id);
        exchangeRateToDelete.setDate(new Date());
        exchangeRateToDelete.setRate(Rate.EUR);
        exchangeRateDao.delete(exchangeRateToDelete);
    }

    @Override
    public ExchangeRateDto update(long id, ExchangeRateDto entity) {
        ExchangeRate exchangeRateToUpdate = convertToExchangeRate(entity);
        exchangeRateToUpdate.setId(id);
        return convertToExchangeRateDto(exchangeRateDao.update(exchangeRateToUpdate));
    }

    @Override
    public void insert(ExchangeRateDto entity) {
        ExchangeRate exchangeRateToInsert = convertToExchangeRate(entity);
        exchangeRateDao.insert(exchangeRateToInsert);
    }

    @Override
    public void insertAll(List<ExchangeRateDto> exchangeRates) {
        for(ExchangeRateDto rateDtoToAdd: exchangeRates){
            insert(rateDtoToAdd);
        }
    }

    private ExchangeRateDto convertToExchangeRateDto(ExchangeRate item){
        return modelMapper.map(item, ExchangeRateDto.class);
    }

    private ExchangeRate convertToExchangeRate(ExchangeRateDto itemDto){
        return modelMapper.map(itemDto, ExchangeRate.class);
    }
}
