package com.labuts.catalog.service.impl;


import com.labuts.catalog.dao.ExchangeRateDao;
import com.labuts.catalog.entity.Item;
import com.labuts.catalog.entity.Rate;
import com.labuts.catalog.entity.dto.ItemDto;
import com.labuts.catalog.dao.ItemDao;
import com.labuts.catalog.service.ItemService;
import com.labuts.catalog.service.exception.IncorrectDataException;
import com.labuts.catalog.service.utils.ItemValidator;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ItemServiceImpl implements ItemService {

    private final ItemDao itemDao;
    private final ExchangeRateDao exchangeRateDao;
    private final ModelMapper modelMapper;
    private final ItemValidator itemValidator;

    @Autowired
    public ItemServiceImpl(ItemDao itemDao, ExchangeRateDao exchangeRateDao,
                           ModelMapper modelMapper, ItemValidator itemValidator) {
        this.itemDao = itemDao;
        this.exchangeRateDao = exchangeRateDao;
        this.modelMapper = modelMapper;
        this.itemValidator = itemValidator;
    }

    @Override
    public List<ItemDto> findAll() {
        List<Item> items = itemDao.findAll();
        return items.stream().map(this::convertToItemDto).collect(Collectors.toList());
    }

    @Override
    public ItemDto findById(long id) {
        Item item = itemDao.findById(id);
        return item == null ? null : convertToItemDto(item);
    }

    @Override
    public void delete(long id) {
        Item itemToDelete = new Item();
        itemToDelete.setId(id);
        itemToDelete.setName("");
        itemDao.delete(itemToDelete);
    }

    @Override
    public ItemDto update(long id, ItemDto entity) throws IncorrectDataException {
        if(itemValidator.isValidItem(entity)){
            Item itemToUpdate = convertToItem(entity);
            itemToUpdate.setId(id);
            return convertToItemDto(itemDao.update(itemToUpdate));
        }else {
            throw new IncorrectDataException("Invalid data for entity to update!");
        }
    }

    @Override
    public void insert(ItemDto entity) throws IncorrectDataException{
        if(!itemValidator.isValidItem(entity)){
            throw new IncorrectDataException("Invalid data for entity to insert!");
        }
        Item itemToInsert = convertToItem(entity);
        itemDao.insert(itemToInsert);
    }

    private ItemDto convertToItemDto(Item item){
        return modelMapper.map(item, ItemDto.class);
    }

    private Item convertToItem(ItemDto itemDto){
        return modelMapper.map(itemDto, Item.class);
    }

    @Override
    public List<ItemDto> findAllInChosenRate(String rate) {
        List<Item> items = itemDao.findAll();
        double coefficient = exchangeRateDao.findLatestRateCoefficient(Rate.valueOf(rate.toUpperCase()));
        items.forEach(item->item.setPrice(item.getPrice()*coefficient));
        return items.stream().map(this::convertToItemDto).collect(Collectors.toList());
    }
}
