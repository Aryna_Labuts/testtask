package com.labuts.catalog.service;

import com.labuts.catalog.entity.dto.ExchangeRateDto;

import java.util.List;

public interface ExchangeRateService extends AbstractService<ExchangeRateDto> {
    void insertAll(List<ExchangeRateDto> exchangeRates);
}
