package com.labuts.catalog.service.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validator {
    static boolean isValid(final String REGULAR_EXPRESSION, String stringToCheck){
        Pattern pattern = Pattern.compile(REGULAR_EXPRESSION);
        Matcher matcher = pattern.matcher(stringToCheck);

        return matcher.matches();
    }
}
