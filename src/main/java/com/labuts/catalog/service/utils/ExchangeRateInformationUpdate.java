package com.labuts.catalog.service.utils;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.labuts.catalog.entity.Rate;
import com.labuts.catalog.entity.dto.ExchangeRateDto;
import com.labuts.catalog.entity.mappers.ExchangeRateMapper;
import com.labuts.catalog.service.ExchangeRateService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.*;

@Service
public class ExchangeRateInformationUpdate {
    private static final Logger logger =
            LoggerFactory.getLogger(ExchangeRateInformationUpdate.class);

    private static final String ACCESS_KEY_VALUE = "ac7a77ef0e3e1ce22758822481082d73";
    private static final String URL_CONNECTION = "http://data.fixer.io/api/latest?access_key=%s";

    private final ObjectMapper objectMapper;
    private final ExchangeRateService exchangeRateService;

    @Autowired
    public ExchangeRateInformationUpdate(ObjectMapper objectMapper, ExchangeRateService exchangeRateService) {
        this.objectMapper = objectMapper;
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        this.exchangeRateService = exchangeRateService;
    }

    @Scheduled(cron = "0 0 0 * * *", zone = "Europe/Minsk")
    public void updateRatesInformation(){
        try {
            RestTemplate restTemplate = new RestTemplate();
            String jsonResult = restTemplate.getForObject(String.format(URL_CONNECTION, ACCESS_KEY_VALUE), String.class);

            JsonNode rootNode = objectMapper.readTree(jsonResult);
            JsonNode success = rootNode.get("success");
            if (success.toString().equals("true")) {
                List<ExchangeRateDto> exchangeRatesToAdd = convert(jsonResult);
                exchangeRateService.insertAll(exchangeRatesToAdd);
                logger.info("Exchange rates have been successfully updated");
            }else {
                logger.info("Impossible to update exchange rates");
            }
        }catch (IOException e) {
            e.printStackTrace();
        }

    }

    private List<ExchangeRateDto> convert(String content){
        List<ExchangeRateDto> result = new ArrayList<>(0);
        try {
            ExchangeRateMapper rateMapper = objectMapper.readValue(content, ExchangeRateMapper.class);
            result = new ArrayList<>(rateMapper.getRates().size());
            Date updateDate = new Date();
            for (Map.Entry<String, Double> entry : rateMapper.getRates().entrySet()) {
                Rate rate = Rate.valueOf(entry.getKey());
                double coefficient = entry.getValue();
                ExchangeRateDto dtoToAdd = new ExchangeRateDto(rate, coefficient, updateDate);
                result.add(dtoToAdd);
            }
        } catch (IOException e) {
            logger.info("Impossible to convert JSON", e);
        }
        return result;
    }

}
