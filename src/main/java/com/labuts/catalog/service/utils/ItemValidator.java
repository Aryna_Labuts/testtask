package com.labuts.catalog.service.utils;

import com.labuts.catalog.entity.dto.ItemDto;
import org.springframework.stereotype.Service;

@Service
public class ItemValidator {
    private static final String ITEM_NAME_REGEX = "[A-Za-z-\\s]{4,50}";
    private static final String ITEM_PRICE_REGEX = "^([1-9]{1}[0-9]{0,5}(\\.[0-9]{0,2})?|0(\\.[1-9]{1}([0-9]{1})?)|0(\\.0[1-9]{1}))$";

    public boolean isValidItem(ItemDto item){
        return item.getName() != null && Validator.isValid(ITEM_NAME_REGEX, item.getName())
                && Validator.isValid(ITEM_PRICE_REGEX, Double.toString(item.getPrice()));
    }
}
